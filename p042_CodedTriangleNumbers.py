# The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1);
# so the first ten triangle numbers are:

# 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

# By converting each letter in a word to a number corresponding to its
# alphabetical position and adding these values we form a word value.
# For example, the word value for SKY is 19 + 11 + 25 = 55 = t10.
# If the word value is a triangle number then we shall call the word a
# triangle word.
from common_math import nth_triangular
import string

word = []
with open("p042_Words.txt", 'r') as f:
    word = f.read().split(',')

max_length = 0
for i in range(len(word) - 1):
    if len(word[i]) > len(word[i + 1]):
        max_length = len(word[i])

triangle_list = [nth_triangular(i) for i in range(1, max_length * 26)]
word_triangle = []

alphabet_dict = {string.ascii_uppercase[i]: (i + 1) for i in range(26)}

for i in word:
    count = 0
    for j in i.strip('"'):
        count += alphabet_dict[j]
    if count in triangle_list:
        word_triangle.append(count)

print(word_triangle.__len__())
