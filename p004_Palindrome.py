# Problem 4
# A palindromic number reads the same both ways. The largest palindrome
# made from the product of two 2-digit numbers is 9009 = 91 × 99.
# Find the largest palindrome made from the product of two 3-digit numbers.

# P4 to find the largest palindrome multiple of two three digit numbers
from common_math import is_palindrome


def lar_palindrome():
    largest = 0
    for i in range(999, 100, -1):
        for j in range(999, 100, -1):
            if is_palindrome(i * j):
                if largest < i * j:
                    largest = i * j

    return largest


print(lar_palindrome())
