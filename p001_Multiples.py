# Problem 1
# If we list all the natural numbers below 10 that are multiples of 3 or 5, we
# get 3, 5, 6 and 9.The sum of these multiples is 23.Find the sum of all the
# multiples of 3 or 5 below 1000.


def sum_multiples(factors, limit):
    """Return the sum of unique multiples of elements in factors list in the
    list of natural numbers less than limit

    >>> sum_multiples([3, 5], 20)
    78
    """
    unique_multiples = []
    # loop through factors
    for factor in factors:
        # loop through natural numbers less than limit
        for num in range(1, limit):
            # if the number is a multiple
            if num % factor == 0:
                # store only unique multiples in the list
                if num not in unique_multiples:
                    unique_multiples.append(num)

    return sum(unique_multiples)


def sum_multiples_lc(limit):
    # return sum([i for i in range(1, limit) if i % 3 == 0 or i % 5 == 0])
    return (sum(range(3, 1000, 3)) + sum(range(5, 1000, 5)) - sum(
        range(15, 1000, 15)))


print(sum_multiples_lc(1000))
print(sum_multiples([3, 5], 1000))
