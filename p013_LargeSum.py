# Problem 13
# Work out the first ten digits of the sum of the following one-hundred
# 50-digit numbers.

summation = 0
f = open("p013_LargeSum.txt", 'r')

for i in range(1, 101):
    summation += int(f.readline())

print(str(summation)[:10])
