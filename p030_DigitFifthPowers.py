# Problem 30
# Surprisingly there are only three numbers that can be written as the sum of
# fourth powers of their digits:

# 1634 = 14 + 64 + 34 + 44
# 8208 = 84 + 24 + 04 + 84
# 9474 = 94 + 44 + 74 + 44
# As 1 = 14 is not a sum it is not included.

# The sum of these numbers is 1634 + 8208 + 9474 = 19316.

# Find the sum of all the numbers that can be written as the sum of fifth
# powers of their digits.

# Method 1
# from itertools import product

# def concatenate_list(_list):
#     num_str = ''
#     for i in _list:
#         num_str = num_str + str(i)
#     return int(num_str)

# powers_dict = {
#     0: 0,
#     1: 1,
#     2: 32,
#     3: 243,
#     4: 1024,
#     5: 3125,
#     6: 7776,
#     7: 16807,
#     8: 32768,
#     9: 59049
# }
# num = 6
# _list = []
# x = list(range(0, 10))

# for i in [p for p in product(x, repeat=num)]:
#     summation = sum([powers_dict[j] for j in i])
#     if concatenate_list(i) == summation:
#         print(concatenate_list(i))
#         _list.append(concatenate_list(i))

# print(sum(_list) - 1)

# Method 2
# If I have a 6-digit number, the biggest sum of digits (to the power of 5)
# I can get is 6 * 9**5 = 354294 which is also a 6-digit number.
# If I have a 7-digit number, the biggest sum I can get is 7 * 9**5 = 413343,
# which is a 6-digit number. So a 7-digit number can never be expressed as the
# sum of its digits ot the power of n. The biggest number that I have to
# search is 354294.


def sum_of_digit(number):
    return sum(int(c)**5 for c in str(number))


total_sum = 0
for i in range(2, 354295):
    if i == sum_of_digit(i):
        total_sum += i

print(total_sum)
