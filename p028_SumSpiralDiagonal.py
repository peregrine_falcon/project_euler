# Problem_28
# Starting with the number 1 and moving to the right in a clockwise direction
# a 5 by 5 spiral is formed as follows:

# 21 22 23 24 25
# 20  7  8  9 10
# 19  6  1  2 11
# 18  5  4  3 12
# 17 16 15 14 13

# It can be verified that the sum of the numbers on the diagonals is 101.

# What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral
# formed in the same way?


def spiral_vector(spiral, x, y, dx, dy, number, k):
    # a row or column vector of the spiral martix that repeats
    for _ in range(k):
        x, y = x + dx, y + dy
        number += 1
        spiral[x][y] = number

    return number, x, y


def create_spiral(n):
    # Constraints
    if n < 1 or n % 2 == 0:
        raise ValueError('{0} is not a positive odd integer'.format(n))

    # Create n x n empty spiral matrix
    spiral = []
    for _ in range(n):
        spiral.append([0] * n)

    # Initialization
    number = 1
    spiral[n // 2][n // 2] = number
    x, y = n // 2, n // 2
    k = 2

    # Spiral logic
    while k < n:  # n
        x, y = x - 1, y + 1
        number, x, y = spiral_vector(spiral, x, y, 1, 0, number, k)
        number, x, y = spiral_vector(spiral, x, y, 0, -1, number, k)
        number, x, y = spiral_vector(spiral, x, y, -1, 0, number, k)
        number, x, y = spiral_vector(spiral, x, y, 0, 1, number, k)
        k += 2

    return spiral


def print_spiral(number):
    spiral = []
    spiral = create_spiral(number)
    for i in spiral:
        print(i)


def diagonal_sum_spiral(number):
    diagonal_sum = 0

    spiral = create_spiral(number)
    for i in range(len(spiral)):
        diagonal_sum += spiral[i][i]

    j = len(spiral) - 1
    for i in range(len(spiral)):
        diagonal_sum += spiral[i][j]
        j -= 1

    return diagonal_sum


print_spiral(3)
print(diagonal_sum_spiral(1001))
