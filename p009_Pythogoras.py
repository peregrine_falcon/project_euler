# Problem 9
# A Pythagorean triplet is a set of three natural numbers,
# a < b < c, for which,

# a2 + b2 = c2
# For example, 32 + 42 = 9 + 16 = 25 = 52.

# There exists exactly one Pythagorean triplet for which
# a + b + c = 1000.
# Find the product abc.

import math
from common_math import list_product


def problem_9():
    c = 1
    for i in range(1, 1000):
        a_square = i**2
        for j in range(1, 1000):
            b_square = j**2
            c = math.sqrt(a_square + b_square)
            if i + j + c == 1000:

                return [i, j, c]


print(int(list_product(problem_9())))
