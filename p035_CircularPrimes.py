# Problem 35
# The number, 197, is called a circular prime
# because all rotations of the digits: 197, 971, and 719, are themselves prime.
# There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71,
# 73, 79, and 97.
# How many circular primes are there below one million?
from common_math import isprime


def rotations(number):
    num_str = str(number)
    rotations = [
        number,
    ]
    for _ in range(len(num_str) - 1):
        swap = num_str[-1] + num_str[:-1]
        rotations.append(int(swap))
        num_str = swap
    return rotations


def is_circular_prime(number):
    if isprime(number):
        for i in rotations(number):
            if not (isprime(i)):
                return False
        return True
    else:
        return False


circular_primes = []
for i in range(10**6):
    if is_circular_prime(i):
        circular_primes.append(i)

print(len(circular_primes))
