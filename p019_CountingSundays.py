# Problem 19
# You are given the following information, but you may prefer
# to do some research for yourself.

# 1 Jan 1900 was a Monday.
# Thirty days has September,
# April, June and November.
# All the rest have thirty-one,
# Saving February alone,
# Which has twenty-eight, rain or shine.
# And on leap years, twenty-nine.

# A leap year occurs on any year evenly divisible by 4, but not
# on a century unless it is divisible by 400.
# How many Sundays fell on the first of the month during the twentieth
# century (1 Jan 1901 to 31 Dec 2000)?

weekdays_dict = {
    1: 'Monday',
    2: 'Tuesday',
    3: 'Wednesday',
    4: 'Thursday',
    5: 'Friday',
    6: 'Saturday',
    7: 'Sunday'
}

months_dict = {
    1: 'Jan',
    2: 'Feb',
    3: 'Mar',
    4: 'Apr',
    5: 'May',
    6: 'Jun',
    7: 'Jul',
    8: 'Aug',
    9: 'Sept',
    10: 'Oct',
    11: 'Nov',
    12: 'Dec'
}

days_month = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31
}


def is_leap_year(year):
    # if year is a century
    if year % 100 == 0:
        return year % 400 == 0
    else:
        return year % 4 == 0


def first_day_year(year):

    years_passed = year - 1900

    # leap year calculations
    leap_years_passed = ((years_passed // 4 - 1) if is_leap_year(year) else
                         (years_passed // 4))
    leap_years_passed = 0 if leap_years_passed < 0 else leap_years_passed

    return ((years_passed * 365 + leap_years_passed) % 7) + 1


def first_day_month(month, year):
    year_first_day = first_day_year(year)

    if month != 1:
        if is_leap_year(year):
            days_passed = sum([days_month[i] for i in range(1, month)])
            if month > 2:
                days_passed += 1
        else:
            days_passed = sum([days_month[i] for i in range(1, month)])

        days = (year_first_day + (days_passed) % 7)
        return days if days == 7 else days % 7

    else:

        return year_first_day


# print(first_day_year(2012))
# year = 2012


def p19():
    count = 0
    for i in range(1901, 2001):
        for j in range(1, 13):
            if first_day_month(j, i) == 7:
                count += 1
    return count


print(p19())
