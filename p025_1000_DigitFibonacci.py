# Problem 25
# The Fibonacci sequence is defined by the recurrence relation:
# Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.

# The 12th term, F12, is the first term to contain three digits.
# What is the index of the first term in the Fibonacci sequence to contain
# 1000 digits?

from common_math import number_split, n_fibonacci_list

i = 1
number = 1000

while (True):
    last_term = n_fibonacci_list(i).pop()
    length = len(number_split(last_term))
    if length == number:
        break
    i += 1

print(i)
