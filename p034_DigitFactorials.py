# Problem 34
# 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

# Find the sum of all numbers which are equal to the sum of the factorial of
# their digits.

# Note: as 1! = 1 and 2! = 2 are not sums they are not included.

from common_math import factorial, number_split


def factorial_sum(number):
    sum = 0
    for i in number_split(number):
        sum += factorial(i)

    return sum


upperbound = 10**6
curious_num = []
for i in range(upperbound):
    if i == factorial_sum(i):
        curious_num.append(i)

print(sum(curious_num) - 3)
