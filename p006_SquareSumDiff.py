# Problem 6
# The sum of the squares of the first ten natural numbers is,
# 12 + 22 + ... + 102 = 385
# The square of the sum of the first ten natural numbers is,

# (1 + 2 + ... + 10)2 = 552 = 3025
# Hence the difference between the sum of the squares of the
# first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

# Find the difference between the sum of the squares of the\
# first one hundred natural numbers and the square of the sum.

# sum of squares
from common_math import sum_ap_series, sum_squares

# square of sum
sq_sum = (sum_ap_series(1, 1, 100))**2
print(abs(sum_squares(1, 1, 100) - sq_sum))
