# Includes important common math functions such as prime, divisors, multiples,
# factorial functions

import math


def primes_list(number):
    """Returns a list of prime numbers that are less than or
    equal to parameter:number (Uses Sieve of Eratosthenes)


    >>> primes_list(10)
    [2, 3, 5, 7]
    """
    if number <= 0:
        # All primes are positive
        raise ValueError("{} is not a positive number".format(number))

    else:
        # Intially assume all values are primes
        primes_bool = [True for i in range(number + 1)]

        # Eliminate all the multiples of numbers
        p = 2
        while (p * p <= number):
            if primes_bool[p] is True:
                for i in range(2 * p, number + 1, p):
                    primes_bool[i] = False
            p += 1

        # Return a list of primes
        return [i for i in range(2, number + 1) if primes_bool[i]]


def isprime(number):
    """Returns boolean of whether parameter:number is a prime

    >>> isprime(11)
    True
    """
    if (number <= 0):
        # All primes are positive
        return False
    elif (number == 1):
        return False
    elif (number < 4):
        return True
    elif (number % 2 == 0):
        return False
    elif (number < 9):
        return True
    elif (number % 3) == 0:
        return False
    else:
        # any number n can have only one primefactor greater than n
        r = math.floor(math.sqrt(number))
        k = 5
        while (k <= r):
            if (number % k == 0):
                return False
            elif (number % (k + 2) == 0):
                return False
            # all primes greater than 3 can be written in the form
            # 6k + 1 or 6k - 1
            k += 6

        return True


def n_primes(n):
    """Returns first n prime numbers

    >>> n_primes(4)
    [2, 3, 5, 7]
    """
    primes = []
    i = 2
    while len(primes) < n:
        if isprime(i):
            primes.append(i)
        i += 1

    return primes


def prime_factors(number):
    """Returns a list of prime factors of the parameter:number

    >>> prime_factors(60)
    [2, 2, 3, 5]
    """
    # check whether the number is prime
    if isprime(number):
        # if prime return [1, number]
        return [1, number]

    factors = []
    # check if number is divisible by 3
    while (number % 2) == 0:
        # loop until all the number is no longer even
        factors.append(2)
        number = number // 2

    # loop through in steps of 2, till sqrt(number)
    for i in range(3, int(math.sqrt(number) + 1), 2):
        # loop through until all the factors are exhausted
        while (number % i == 0):
            factors.append(i)
            number = number // i

    # if a number remains then it must be prime factor greater than
    # sqrt(number)
    if number > 2:
        factors.append(number)

    return factors


def prime_factors_dict(number):
    """Returns a dictionary of prime factors

    >>> prime_factors_dict(60)
    {2: 2, 3: 1, 5: 1}
    """
    factors_dict = {}
    factors = prime_factors(number)
    if isprime(number):
        return {1: 1, number: 1}

    # loop through the list to count the number of factors
    for factor_i in factors:
        count = 0
        for factor_j in factors:
            if factor_i == factor_j:
                count += 1
        factors_dict[factor_i] = count

    return factors_dict


def num_divisors(number):
    """Returns the number of divisors for the number

    >>> num_divisors(250891200)
    504
    """
    exponents = prime_factors_dict(number).values()
    product = 1

    # total divisors by multiplication theorem
    for num in exponents:
        product = product * (num + 1)

    return product


def divisors_list(number):
    """Returns the list of divisors for the number

    >>> divisors_list(28)
    [1, 2, 4, 7, 14, 28]
    """
    # merge [1, number] and unique prime factors
    divisors = [1, number]
    for i in prime_factors(number):
        if i not in divisors:
            divisors.append(i)

    # loop through divisors list to find products which are less than
    # number. Note: The list updates after every iteration.
    for divisor_i in divisors:
        for divisor_j in divisors:
            product = divisor_i * divisor_j
            if product < number:
                if number % product == 0:
                    if product not in divisors:
                        divisors.append(product)
    divisors.sort()

    return divisors


def multiples(factors, limit):
    """Return the dictionary of multiples of elements in factors list in the
    list of natural numbers less than limit

    >>> multiples([3, 5], 10)
    {3: [3, 6, 9], 5: [5]}
    """
    factor_dict = {}
    # loop through factors
    for factor in factors:
        # iterate through natural numbers less than limit
        for num in range(1, limit):
            # if the number is a multiple
            if num % factor == 0:
                # store it in dictionary
                if factor not in factor_dict:
                    factor_dict[factor] = []
                factor_dict[factor].append(num)

    return factor_dict


def collatz_seq(number):
    """Returns a list of collatz sequence for the number

    >>> collatz_seq(10)
    [10, 5, 16, 8, 4, 2, 1]
    """
    collatz = [
        number,
    ]
    if number <= 0:
        raise ValueError("{}, is not a positive integer".format(number))

    while (number != 1):
        if number % 2 == 0:
            number = number // 2
            collatz.append(number)
        else:
            number = 3 * number + 1
            collatz.append(number)

    return collatz


def fibonacci_genetator(limit):
    """Generates a list of fibonacci values that are less than limit

    >>> fibonacci_generator(25)
    [1, 1, 2, 3, 5, 8, 13, 21]
    """
    a, b = 1, 1
    seq = [
        1,
    ]
    while b < limit:
        seq.append(b)
        a, b = b, a + b

    return seq


def n_fibonacci_list(number):
    """Generates a list of parameter:number fibonacci values

    >>> n_fibonacci_list(8)
    [1, 1, 2, 3, 5, 8, 13, 21]
    """
    fibonacci = []
    x, y = 1, 1
    while len(fibonacci) < number:
        if len(fibonacci) < number:
            fibonacci.append(x)
        else:
            break
        if len(fibonacci) < number:
            fibonacci.append(y)
        else:
            break
        if len(fibonacci) < number:
            fibonacci.append(x + y)
        else:
            break
        x, y = x + 2 * y, 2 * x + 3 * y

    return fibonacci


def number_split(number):
    """Splits the number into a list of digits

    >>> number_split(9009)
    [9, 0, 0, 9]
    """
    split_list = []
    for _ in range(len(str(number))):
        split_list.append(number % 10)
        number = number // 10
    split_list.reverse()

    return split_list


def nth_triangular(n):
    """Returns nth triangular number

    >>> nth_triangular(9)
    45
    """

    return n * (n + 1) // 2


def factorial(number):
    """Returns factorial of number

    >>> factorial(4)
    24
    """
    product = 1
    for i in range(1, number + 1):
        product = product * i

    return product


def is_palindrome(number):
    """Checks whether the given number is a palindrome of not

    >>> is_palindrome(9009)
    True
    """
    flag = 0
    numbers = number_split(number)
    length = len(numbers)
    for i in range(length // 2):
        if numbers[i] != numbers[length - i - 1]:
            flag = 1
            break

    return flag == 0


def ap_series(a, d, n):
    """Returns a list of arithmetic progressions
    a -> first term
    d -> common difference
    n -> number of terms

    >>> ap_series(3, 2, 4)
    [3, 5, 7, 9]
    """
    series = [a]
    for _ in range(1, n):
        cur_term = a + d
        series.append(cur_term)
        a = cur_term

    return series


def sum_ap_series(a, d, n):
    """Returns the sum of elements in the given arithmetic progression
    a -> first term
    d -> common difference
    n -> number of terms

    >>> sum_ap_series(3, 2, 4)
    24
    """

    return int((n / 2) * (2 * a + (n - 1) * d))


def sum_squares(a, d, n):
    """Returns the sum of squares of elements of in the given arithmetic
    progression
    a -> first term
    d -> common difference
    n -> number of terms

    >>> sum_squares(3, 2, 4)
    164
    """
    sum_ = 0
    series = ap_series(a, d, n - 1)
    for num in series:
        sum_ += num**2

    return sum_


def list_product(_list):
    """Returns the product of the elements of a list

    >>> list_product([1, 2, 3, 4])
    24
    """
    product = 1
    for i in _list:
        product = product * i
    return product


def recurring_remainders(number):
    """Prints out the recurring remainders for the fraction 1 / number
    where number is a positive integer greater than 1

    >>> recurring_remainders(7)
    [1, 3, 2, 6, 4, 5]
    """
    if number < 2:
        raise ValueError('{0}, is not a postive number > 2'.format(number))
    recur_rem = []
    cur = 1
    while True:
        prev = cur % number
        if prev == 0:
            break

        cur = (prev * 10) % number
        if cur == 0:
            break
        if cur == prev:
            break
        if cur in recur_rem:
            break

        recur_rem.append(prev)
        recur_rem.append(cur)
        cur = cur * 10
    return recur_rem
