# Problem 22
# Using names.txt (right click and 'Save Link/Target As...'), a 46K text file
# containing over five-thousand first names, begin by sorting it into
# alphabetical order. Then working out the alphabetical value for each name,
# multiply this value by its alphabetical position in the list to obtain a
# name score.

# For example, when the list is sorted into alphabetical order, COLIN,
# which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list.
# So, COLIN would obtain a score of 938 × 53 = 49714.

# What is the total of all the name scores in the file?

import string
alphabet_dict = {string.ascii_uppercase[i]: (i + 1) for i in range(26)}
f = open("p022_names.txt", 'r')
names = sorted([n.strip('"') for n in f.read().split(',')])
summation = 0
i = 1

for name in names:
    name_sum = 0

    for char in name:
        name_sum += alphabet_dict.get(char)

    summation += name_sum * i
    i += 1

print(summation)
