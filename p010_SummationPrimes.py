# Problem 10
# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
# Find the sum of all the primes below two million.
from common_math import primes_list
print(sum([i for i in primes_list(2000000)]))
