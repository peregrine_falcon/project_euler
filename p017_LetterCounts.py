# Problem 18
# If the numbers 1 to 5 are written out in words: one, two, three, four, five,
# then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

# If all the numbers from 1 to 1000 (one thousand) inclusive were written
# out in words, how many letters would be used?

# NOTE: Do not count spaces or hyphens. For example,
# 342 (three hundred and forty-two) contains 23 letters
# and 115 (one hundred and fifteen) contains 20 letters.
# The use of "and" when writing out numbers is in compliance with British
# usage.


def numbers_words(number):
    # Upto 1000

    digits_dict = {
        1: 'one',
        2: 'two',
        3: 'three',
        4: 'four',
        5: 'five',
        6: 'six',
        7: 'seven',
        8: 'eight',
        9: 'nine',
        10: 'ten',
        11: 'eleven',
        12: 'twelve',
        13: 'thirteen',
        14: 'fourteen',
        15: 'fifteen',
        16: 'sixteen',
        17: 'seventeen',
        18: 'eighteen',
        19: 'nineteen'
    }
    tens_dict = {
        20: 'twenty',
        30: 'thirty',
        40: 'forty',
        50: 'fifty',
        60: 'sixty',
        70: 'seventy',
        80: 'eighty',
        90: 'ninety'
    }

    if 1 <= number <= 19:
        return digits_dict.get(number)
    elif 19 < number < 100:
        # 20 to 99
        ones = number % 10
        tens = (number // 10) * 10
        if ones != 0:
            return tens_dict.get(tens) + ' ' + digits_dict.get(ones)
        else:
            return tens_dict.get(tens)
    elif 100 <= number < 1000:
        ones = number % 10
        tens = (number // 10) % 10
        hundreds = number // 100
        if ones != 0:
            if tens >= 2:
                return (digits_dict.get(hundreds) + ' hundred and ' +
                        tens_dict.get(tens * 10) + ' ' + digits_dict.get(ones))
            else:
                return (digits_dict.get(hundreds) + ' hundred and ' +
                        digits_dict.get(int(str(tens) + str(ones))))

        elif ones == 0 and tens != 0:
            return (digits_dict.get(hundreds) + ' hundred and ' +
                    tens_dict.get(tens * 10, digits_dict.get(tens * 10)))

        elif ones == 0 and tens == 0:
            return digits_dict.get(hundreds) + ' ' + 'hundred'

    elif number == 1000:
        return 'one thousand'


summation = 0
for i in range(1, 1001):
    word = str(numbers_words(i))
    summation += len(word.replace(' ', ''))

print(summation)
