# Problem 15
# Starting in the top left corner of a 2×2 grid,
# and only being able to move to the right and down,
# there are exactly 6 routes to the bottom right corner.

# How many such routes are there through a 20×20 grid?

# There are only two routes
# Down represented by D and right represented by R
# to go from start on the left most(0, 0) pit to the end (n, n)
# there are a total of 2 * n possibilities
# out of the 2n possiblities there can be only two routes with n repititions
# each
# formula to find the total number of paths
# Number of routes = (2n!) / (n!) * (n!)

from common_math import factorial
# size
n = 20
print(factorial(2 * n) // (factorial(n) * factorial(n)))
