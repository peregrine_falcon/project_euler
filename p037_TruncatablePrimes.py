# Problem 37
# The number 3797 has an interesting property. Being prime itself, it is
# possible to continuously remove digits from left to right, and remain prime
# at each stage: 3797, 797, 97, and 7. Similarly we can work from right to
# left: 3797, 379, 37, and 3.

# Find the sum of the only eleven primes that are both truncatable from left
# to right and right to left.

# NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
from common_math import isprime


def is_truncate_list_prime(number):
    num_str = str(number)
    truncates = []

    for i in range(1, len(num_str)):
        # truncate from both left and right
        truncates.append(int(num_str[:-i]))
        truncates.append(int(num_str[i:]))

    for i in truncates:
        if not (isprime(i)):
            return False

    return True


# print(is_truncate_list_prime(3797))

primes = []

i = 8
while len(primes) != 11:
    if isprime(i):
        if is_truncate_list_prime(i):
            primes.append(i)
    i += 1

print(sum(primes))
