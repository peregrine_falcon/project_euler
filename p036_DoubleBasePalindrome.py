# Problem 36
# The decimal number, 585 = 10010010012 (binary), is palindromic in both bases.

# Find the sum of all numbers, less than one million, which are palindromic in
# base 10 and base 2.

# (Please note that the palindromic number, in either base, may not include
# leading zeros.)
from common_math import is_palindrome

palindromes = []

for i in range(1, 10**6):
    if is_palindrome(i):
        if is_palindrome(int(f"{i:b}")):
            palindromes.append(i)

# for i in palindromes:
#     print(i, f"{i:b}")

print(sum(palindromes))
