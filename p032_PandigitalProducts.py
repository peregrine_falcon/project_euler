# Problem 32
# We shall say that an n-digit number is pandigital if it makes use of all the
# digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1
# through 5 pandigital.

# The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing
# multiplicand, multiplier, and product is 1 through 9 pandigital.

# Find the sum of all products whose multiplicand/multiplier/product identity
# can be written as a 1 through 9 pandigital.

# HINT: Some products can be obtained in more than one way so be sure to only
# include it once in your sum.

from common_math import number_split


def is_pandigital(number):
    num_list = sorted(number_split(number))
    for i in range(len(num_list)):
        if len(num_list) > 9:
            return False
        if i + 1 != num_list[i]:
            return False

    return True


# The product of the number should be 1 to 9 pandigital
# According to problem this needs to be in the form multiplicand and multiplie
# This can be only possible by the multiplication of the following
# one-digit number with a four-digit number
# two-digit number with a three-digit number

pandigital_dict = {}

# one-digit number with a four-digit number
for i in range(1, 10):
    for j in range(1000, 10000):
        product = i * j
        number = int(str(product) + str(i) + str(j))
        if is_pandigital(number):
            pandigital_dict[product] = [i, j]

# two-digit number with a three-digit number
for i in range(10, 100):
    for j in range(100, 1000):
        product = i * j
        number = int(str(product) + str(i) + str(j))
        if is_pandigital(number):
            pandigital_dict[product] = [i, j]

print(sum(pandigital_dict.keys()))
